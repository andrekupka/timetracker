# timetracker

A web-application for time-tracking. Written in Flask and Ember.

## Frontend

The frontend can be found [here](https://gitlab.com/freakout/timetracker-frontend).

## Backend

The backend can be found [here](https://gitlab.com/freakout/timetracker-backend).
